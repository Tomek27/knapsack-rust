# Knapsack Rust

This is a small and simple program to calculate the best solution
for the **0-1** [Knapsack Problem](https://en.wikipedia.org/wiki/Knapsack_problem).

## Running
```
knapsack path [num_threads]
```

### Arguments
* `path` Path to a file with the input
* `num_threads` Number of threads to calculate the problem on in parallel

---------------------------------------------------------------

## Input file layout
First line of the file holds the knapsack max weight.
Every further line has two numbers separated by a space.
First number is the item's weight and second is the item's value.

### Example file
```txt
42
2 7
9 32
1 3
23 5
```