//! Some functions to calculate the best solution for the knapsack problem
use std::fmt;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;
use std::sync::Arc;
use std::sync::mpsc;
use std::thread;
use std::usize;
use std::vec::Vec;

/// Holds values for an item in the knapsack problem
#[derive(Debug, Copy, Clone)]
pub struct Item
{
    /// Weight of the item
    pub weight: u64,
    /// Value of the item
    pub value: u64,
}

/// Holds values for a knapsack in the knapsack problem
#[derive(Debug)]
pub struct Knapsack
{
    /// Weight of the knapsack based on its items.
    /// **Note:** In case of the input knapsack, it's the max weight.
    pub weight: u64,
    /// Value of the knapsack based on its items.
    /// **Note:**
    /// In case of the input knapsack, it's zero.
    pub value: u64,
    /// Items choosen for the knapsack.
    /// **Note:**
    /// In case of the input knapsack, it contains all items.
    pub items: Vec<Item>,
}

/// Running the calculation for the knapsack problem
///
/// # Parameters
/// * `items` Items to select from for the knapsack
/// * `knapsack_weight` Max weight of the knapsack
/// * `num_threads` Number of threads to run the calculation on
///
/// # Returns
/// A knapsack with the best solution
pub fn run(items: Vec<Item>, knapsack_weight: u64, num_threads: usize) -> Result<Knapsack, MaxNumPermutationsError> {
    // Convert items to an Atomic Reference Counter
    let items = Arc::new(items);
    let num_permutations = 1usize.wrapping_shl(items.len() as u32);

    // Check if the num_permutations wrapped around
    // and therefore the calculation cannot be executed
    if num_permutations == 1usize {
        return Err(MaxNumPermutationsError);
    }

    println!("Total Permutations: {} @ {} threads", num_permutations, num_threads);
    println!("Calculating...");

    let permutations_per_thread = num_permutations / num_threads;
    let mut permutations_left = num_permutations;
    let mut from_permutation = 0;
    let mut to_permutation = permutations_per_thread;

    let (sender, receiver) = mpsc::channel();

    // Assigning parts of all permutations for the solution
    // Spawning threads to calculate the best solution for the assigned parts
    for _ in 0..num_threads {
        let from = from_permutation;
        let to = to_permutation;
        let sender = sender.clone();
        let items = items.clone();

        thread::spawn(move || {
            let mut best_permutation = from;
            let mut best_value = 0u64;

            // Each bit of the permutation from right to left matches
            // an item at the same position in `items` as the bit itself.
            // A 1 means this item should be taking into account
            // for the sum of weight and value of the current knapsack, otherwise not.
            'perm: for permutation in from..to {
                let mut curr_knapsack_weight = 0u64;
                let mut curr_knapsack_value = 0u64;

                for position in 0..items.len() {
                    // `mask` creates a number with just one bit set
                    // at the corresponding position
                    let mask = 1usize << position;

                    // Checks if at this position in the permutation
                    // the item is set to be taken into account
                    if (permutation & mask) != 0 {
                        curr_knapsack_weight += items[position].weight;
                        curr_knapsack_value += items[position].value;
                    }

                    // Skip this permutation, as it is not a valuable solution,
                    // because its weight exceeds the max knapsack weight
                    if curr_knapsack_weight > knapsack_weight {
                        continue 'perm;
                    }
                }

                // Update the best solution
                if curr_knapsack_value > best_value {
                    best_permutation = permutation;
                    best_value = curr_knapsack_value;
                }
            } // perm

            sender.send((best_permutation, best_value)).expect("Failed to send result");
        });

        permutations_left -= permutations_per_thread;
        from_permutation = to_permutation;
        to_permutation = if permutations_left >= permutations_per_thread {
            to_permutation + permutations_per_thread
        } else {
            permutations_left
        };
    }

    // Collecting all results from the threads
    let mut best_permutation_threads = Vec::with_capacity(num_threads);
    for _ in 0..num_threads {
        best_permutation_threads.push(receiver.recv().expect("Failed to receive result from thread"));
    }

    // Linear search for best permutation of all threads
    let mut best_idx = 0usize;
    for i in 0..best_permutation_threads.len() {
        if best_permutation_threads[i].1 > best_permutation_threads[best_idx].1 {
            best_idx = i;
        }
    }

    // Return the best knapsack with its items, weight and value
    Ok(knapsack_from_permutation(items, best_permutation_threads[best_idx].0))
}

/// This error is returned if the max number of permutations exceeds
/// the MAX value of usize, which is using each bit as a representation
/// of each item at the bit's position.
#[derive(Debug)]
pub struct MaxNumPermutationsError;

/// Loads a file with the input knapsack
///
/// # Parameters
/// * `path` Path to the file with the input knapsack
///
/// # Returns
/// Tuple as input for the knapsack calculation consisting of
/// `(Knapsack Max Weight, Items)`
pub fn load_text_file(path: &Path) -> (u64, Vec<Item>) {
    let f = File::open(path).expect("Failed to open file");
    let mut file = BufReader::new(&f);

    let mut knapsack_line = String::new();
    file.read_line(&mut knapsack_line).expect("Failed to read line");

    let mut split = knapsack_line.split_whitespace();
    let knapsack_weight = split.next().expect("Failed to obtain knapsack weight");
    let knapsack_weight = knapsack_weight.parse::<u64>().expect("Failed to parse knapsack weight");

    let mut items = Vec::<Item>::new();

    for line in file.lines() {
        let line = line.expect("Failed to obtain line");
        let mut split = line.split_whitespace();
        let weight = split.next().expect("Failed to obtain item weight");
        let value = split.next().expect("Failed to obtain item value");
        let weight = weight.parse::<u64>().expect("Failed to parse item weight");
        let value = value.parse::<u64>().expect("Failed to parse item value");

        items.push(Item { weight, value });
    }
    items.shrink_to_fit();
    (knapsack_weight, items)
}

/// For given `items` and `permutations` returns the according knapsack.
fn knapsack_from_permutation(items: Arc<Vec<Item>>, permutation: usize) -> Knapsack {
    let mut knapsack = Knapsack { weight: 0, value: 0, items: Vec::<Item>::new() };

    for position in 0..items.len() {
        let mask = 1usize << position;
        if (permutation & mask) != 0 {
            knapsack.weight += items[position].weight;
            knapsack.value += items[position].value;
            knapsack.items.push(items[position].clone());
        }
    }

    knapsack
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\t{}", self.weight, self.value)
    }
}

impl fmt::Display for Knapsack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Weight: {}\tValue: {}", self.weight, self.value)?;
        writeln!(f, "Items:\nWeight\tValue")?;
        self.items.iter().for_each(|i| {
            let _ = writeln!(f, "{}", i);
        });
        write!(f, "")
    }
}
