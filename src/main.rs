extern crate clap;
extern crate time;

pub mod knapsack;

use clap::{App, Arg};
use std::path::Path;
use std::process;

const PATH_ARG: &str = "path";
const NUM_THREADS_ARG: &str = "num_threads";
const MIN_NUM_THREADS: usize = 1;

fn main() {
    let path_arg = Arg::with_name(PATH_ARG).takes_value(true).required(true)
        .help("Path to the file with the input knapsack");
    let num_threads_arg = Arg::with_name(NUM_THREADS_ARG).takes_value(true).required(false)
        .help("Number of threads for parallel execution >=1");
    let mut app = App::new("knapsack")
        .version("0.1.0")
        .about("Calculates the best solution for the knapsack problem")
        .arg(path_arg).arg(num_threads_arg);

    let matches = app.clone().get_matches();
    let path = matches.value_of(PATH_ARG);
    let num_threads = matches.value_of(NUM_THREADS_ARG);

    // Convert into appropriate types
    let path = Path::new(path.expect("Failed to obtain the path"));
    let num_threads = num_threads.unwrap_or("1").parse::<usize>().expect("Failed to parse number of threads");

    // Check if file exists and for valid number of threads
    if !path.exists() {
        app.print_help().unwrap();
        eprintln!("\nPath to file does not exists");
        process::exit(-1);
    } else if num_threads < MIN_NUM_THREADS {
        app.print_help().unwrap();
        eprintln!("\nWrong number of threads");
        process::exit(-2);
    }

    println!("\nRunning with parameters:");
    println!("Path: {:?}", path);
    println!("Num threads: {}", num_threads);

    let (knapsack_weight, items) = knapsack::load_text_file(path);
    let start_time = time::precise_time_ns();

    let best_knapsack = knapsack::run(items, knapsack_weight, num_threads);

    let end_time = time::precise_time_ns();

    if best_knapsack.is_err() {
        println!("Best solution could not be calculated: {:?}", best_knapsack.unwrap_err());
    } else {
        println!("\nBest knapsack:\n{}", best_knapsack.unwrap());
        print_time(start_time, end_time);
    }
}

fn print_time(start_time: u64, end_time: u64) {
    let duration = end_time - start_time;
    println!("Duration:");
    println!("{} ns", duration);
    if duration > 1_000 {
        println!("{} µs", duration / 1_000);
    }
    if duration > 1_000_000 {
        println!("{} ms", duration / 1_000_000);
    }
    if duration > 1_000_000_000 {
        println!("{} s", duration / 1_000_000_000);
    }
}
